﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns
{
    using System;

    namespace wzorzecadapter
    {
        class Program
        {
            public class OldClass
            {
                public void desc(string info)
                {
                    Console.WriteLine("From Old class! " + info);
                }

            }

            public interface NewInterface
            {
                void Note(string msg);
            }

            public class NewClass : NewInterface
            {
                protected OldClass klasa = null;

                public NewClass(OldClass klasa)
                {
                    this.klasa = klasa;
                }
                public void Note(string wpis)
                {
                    this.klasa.desc(wpis);
                }

            }
            /*
            static void Main(string[] args)
            {
                NewInterface newObj = new NewClass(new OldClass());
                newObj.Note("Info from main");
                Console.ReadKey();
            }*/
        }
    }
}
