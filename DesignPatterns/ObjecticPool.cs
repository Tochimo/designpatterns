﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns
{
    class KrankenHaouse<T> where T : new()
    {
        private static List<T> _availablePointHelp = new List<T>();
        private List<T> _inUse = new List<T>();

        public int counter = 0;
        private int MAXTotalHelps;

        private static KrankenHaouse<T> instance = null;

        public static KrankenHaouse<T> GetInstance()
        {
            lock (_availablePointHelp)
            {
                if (instance == null)
                {
                    instance = new KrankenHaouse<T>();
                }
                else
                {
                    Console.WriteLine("This is singleton, and alredy existig!");
                }
                return instance;
            }
        }

        public T GiveHelp()
        {
            lock (_availablePointHelp)
            {
                if (_availablePointHelp.Count != 0 && _inUse.Count < 10)
                {
                    T item = _availablePointHelp[0];
                    _inUse.Add(item);
                    _availablePointHelp.RemoveAt(0);
                    counter--;
                    return item;
                }
                else
                {
                    T obj = new T();
                    _inUse.Add(obj);
                    return obj;
                }
            }
        }

        public void ReleaseHelp(T item, string caller)
        {
            if (counter <= MAXTotalHelps)
            {
                _availablePointHelp.Add(item);
                counter++;
                _inUse.Remove(item);
            }
            else
            {
                Console.WriteLine("Can't help all intervention is in use! "+ caller);
            }
        }

        public void SetMaxPoolSize(int settingPoolSize)
        {
            MAXTotalHelps = settingPoolSize;
        }
    }

    interface IMecialIntervenation
    {
        void IfIMecialIntervenationAvailable(string caller);
        void IfIMecialIntervenationNoAvailable(string caller);
    }

    class HelpSpace : IMecialIntervenation
    {
        public void IfIMecialIntervenationAvailable(string caller)
        {
            Console.WriteLine("Mediacl available "+ caller);
        }

        public void IfIMecialIntervenationNoAvailable(string caller)
        {
            Console.WriteLine("Mediacl not available "+ caller);
        }
    }

    class MedicalHelp
    {
        public int intervenation;
        public KrankenHaouse<HelpSpace> objPool;

        public void IntervenationWorker()
        {
            intervenation++;
        }

        public void OrderHelp()
        {
            objPool = KrankenHaouse <HelpSpace>.GetInstance();
            objPool.SetMaxPoolSize(1);
        }

        public int Intervenation()
        {
            return intervenation;
        }

        public void SendeMecialIntervenation(HelpSpace obj, string caller)
        {
            intervenation--;
            objPool.ReleaseHelp(obj, caller);
        }

        public void CheckThatifIntevertionAvailable(HelpSpace obj, bool ifIntevertionAvailable, string caller)
        {
            if (ifIntevertionAvailable == true)
            {
                obj.IfIMecialIntervenationAvailable( caller);
            }
            else if (ifIntevertionAvailable == false)
            {
                obj.IfIMecialIntervenationNoAvailable( caller);
            }
        }
    }




}
