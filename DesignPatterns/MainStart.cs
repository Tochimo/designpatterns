﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DesignPatterns;
namespace DesignPatterns
{
    class MainStart
    {
        static void Main()
        {
            Console.WriteLine("---------------Choose Design Patters----------------");
            Console.WriteLine("---------------1 Singleton----------------");
            Console.WriteLine("---------------2 Bulider----------------");
            Console.WriteLine("---------------3 Object Pool----------------");
            Console.WriteLine("---------------4 Decorater----------------");
            Console.WriteLine("---------------Q Exit----------------");

            String info = Console.ReadLine();
            while (info != "q")
            {
                if (info == "1")
                {
                    Console.WriteLine("---------------Singleton1----------------");
                    Singleton1.Method();
                    Singleton1.Property = "Field data determined in the Program.Main method";
                    Singleton1.Method();
                    Console.WriteLine("---------------Singleton2----------------");
                    Singleton2.Method();
                    Singleton2.IstSigle().Property = "Field data determined in the Program.Main method 2";
                    /*
                    Singleton2 referencja = Singleton2.IstSigle();
                    referencja.Property = "Field data determined in the Program.Main method 2";
                    */
                    Singleton2.Method();
                    Console.WriteLine("-------------------------------");
                    Console.WriteLine("Press any key to exit.");
                }
                else if (info == "2")
                {
                    Console.WriteLine("-------------------------------");
                    Console.WriteLine("---------------Bulider----------------");
                    MasterGame mg = new MasterGame();
                    mg.setCreate(new Enemy());
                    mg.create("GrotztugA", 150, 5, "Berserk", "Axe");
                    mg.getCreaterNPC().show();
                    Console.WriteLine("---------------Bulider2----------------");
                    mg.setCreate(new Allay());
                    mg.create("Theth", 15, 11, "dsadsadas", "Sword");
                    mg.getCreaterNPC().show();
                    Console.WriteLine("Press any key to exit.");
                }
                else if (info == "3")
                {
                    bool isava = true;
                    Console.WriteLine("------------Object Pool-------------------");
                    Task task1 = Task.Run(() =>
                    {
                        MedicalHelp help = new MedicalHelp();
                        help.IntervenationWorker();
                        help.OrderHelp();
                        HelpSpace obj = help.objPool.GiveHelp();
                        Console.WriteLine("The value of the intevention in progres task1: " + help.objPool.counter);
                        isava= help.objPool.counter < 1 ? true : false;
                        help.CheckThatifIntevertionAvailable(obj, isava,"1");
                        //if help.intervenation
                        help.SendeMecialIntervenation(obj,"1");
                       
                        Console.WriteLine("The value of the intevention in progres second for 1: " + help.objPool.counter);
                        isava = help.objPool.counter < 1 ? true : false;
                        help.CheckThatifIntevertionAvailable(obj, isava, "1");

                    });
                     Task task2 = Task.Run(() =>
                     {
                         MedicalHelp help = new MedicalHelp();
                         help.IntervenationWorker();
                         help.OrderHelp();
                         HelpSpace obj = help.objPool.GiveHelp();
                         Console.WriteLine("The value of the intevention in progres task2: " + help.objPool.counter);
                         isava = help.objPool.counter < 1 ? true : false;
                         help.CheckThatifIntevertionAvailable(obj, isava,"2");
                         //if help.intervenation
                          help.SendeMecialIntervenation(obj,"2");
                         Console.WriteLine("The value of the intevention in progres second for 2: " + help.objPool.counter);
                         isava = help.objPool.counter < 1 ? true : false;
                         help.CheckThatifIntevertionAvailable(obj, isava, "2");
                     });
                      
                }
                else if (info == "4")
                {

                    NPCStats allay = new AllayStatus();
                   NPCStats enemy = new EnemyStatus();
                    Console.WriteLine("\nCore stats allay");
                    Console.WriteLine(allay.about() + " " + allay.hp());
                    Console.WriteLine(allay.about() + " " + allay.spped());
                    Console.WriteLine(allay.about() + " " + allay.stenght());
                    Console.WriteLine(allay.about() + " " + allay.agility());
                    Console.WriteLine("\nCore stats enemy");
                    Console.WriteLine(enemy.about() + " " + enemy.hp());
                    Console.WriteLine(enemy.about() + " " + enemy.spped());
                    Console.WriteLine(enemy.about() + " " + enemy.stenght());
                    Console.WriteLine(enemy.about() + " " + enemy.agility());
                    Console.WriteLine("\nIts dark use Light");
                    allay = new Light(allay);
                    enemy = new Light(enemy);
                    Console.WriteLine("\nZ Effect:");
                    Console.WriteLine("\nLight stats allay");
                    Console.WriteLine(allay.about() + " " + allay.hp());
                    Console.WriteLine(allay.about() + " " + allay.spped());
                    Console.WriteLine(allay.about() + " " + allay.stenght());
                    Console.WriteLine(allay.about() + " " + allay.agility());
                    Console.WriteLine("\nLight stats enemy");
                    Console.WriteLine(enemy.about() + " " + enemy.hp());
                    Console.WriteLine(allay.about() + " " + enemy.spped());
                    Console.WriteLine(allay.about() + " " + enemy.stenght());
                    Console.WriteLine(allay.about() + " " + enemy.agility());
                    Console.WriteLine("\nUse fireball, taget: all, Light still on");
                    allay = new FireBall(allay);
                    enemy = new FireBall(enemy);
                    Console.WriteLine("\nZ Effect:");
                    Console.WriteLine("\nLight & Fierball stats allay");
                    Console.WriteLine(allay.about() + " " + allay.hp());
                    Console.WriteLine(allay.about() + " " + allay.spped());
                    Console.WriteLine(allay.about() + " " + allay.stenght());
                    Console.WriteLine(allay.about() + " " + allay.agility());
                    Console.WriteLine("\nLight $ Fierball stats enemy");
                    Console.WriteLine(enemy.about() + " " + enemy.hp());
                    Console.WriteLine(enemy.about() + " " + enemy.spped());
                    Console.WriteLine(enemy.about() + " " + enemy.stenght());
                    Console.WriteLine(enemy.about() + " " + enemy.agility());
                   

                }




         
                info = Console.ReadLine();
            }
          



        }
    }
}
