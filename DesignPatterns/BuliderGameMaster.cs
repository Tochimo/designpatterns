﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns
{


    class NPC
    {
        private String name;
        private int hp;
        private int power;
        private int level;
        private String classType;
        private String weapons;
        private String frendly;
        public String[] myClassArrEnemy = new String[] { "Berserk", "Shaman", "Thief", "Archer", "None" };
        public String[] myClassArrAllay = new String[] { "Paladin", "Wizzard", "Scout", "Ranger", "None" };

        public void setName(string name)
        {
            this.name = name;
        }

        public void setHP(int hp)
        {
            this.hp = hp;
        }

        public void setPower(int power)
        {
            this.power = power;
        }

        public void setLevel(int level)
        {
            this.level = level;
        }

        public void setclassType(String classType)
        {
            this.classType = classType;
        }

        public void setweapons(String weapons)
        {
            this.weapons = weapons;
        }
        public void setFrendly(string frendly)
        {
            this.frendly = frendly;
        }

        public void show()
        {
            if (name != null) Console.WriteLine("Name = " + name);
            if (hp != 0) Console.WriteLine("HP = " + hp);
            if (power != 0) Console.WriteLine("Power = " + power);
            if (level != 0) Console.WriteLine("Level = " + level);
            if (classType != null) Console.WriteLine("Class = " + classType);
            if (weapons != null) Console.WriteLine("Weapons = " + weapons);
            Console.WriteLine("Frendly = " + frendly);
        }

    }

    abstract class NPCCreator
    {
        protected NPC npcdefalut;

        public void newNPC()
        {
            npcdefalut = new NPC();
        }
        public NPC getCreaterNPC()
        {
            return npcdefalut;
        }
        public abstract void buildName(string name);
        public abstract void buildHP(int hp);
        public abstract void buildPower(int power);
        public abstract void buildLevel();
        public abstract void buildClassType(string classType);
        public abstract void buildWeapons(string weapons);
        public abstract void bulidFrendly();
        //DatabaseMaybe?

    }

    class Enemy : NPCCreator
    {
        public override void buildName(string name)
        {
            npcdefalut.setName(name);
        }
        public override void buildHP(int hp)
        {
            npcdefalut.setHP((hp>100) ? 100 : hp);
        }
        public override void buildPower(int power)
        {
            npcdefalut.setPower((power > 10) ? 10 : power);
        }
        public override void buildLevel()
        {
            npcdefalut.setLevel(1);
        }

        public override void buildClassType(string classType)
        {
            npcdefalut.setclassType((Array.IndexOf(npcdefalut.myClassArrEnemy, classType) != -1) ? classType : npcdefalut.myClassArrEnemy[4].ToString());
        }
        public override void buildWeapons(string weapons)
        {
            npcdefalut.setweapons(weapons);
        }

        public override void bulidFrendly()
        {
            npcdefalut.setFrendly("false");
        }
    }

    class Allay : NPCCreator
    {
        public override void buildName(string name)
        {
            npcdefalut.setName(name);
        }
        public override void buildHP(int hp)
        {
            npcdefalut.setHP((hp > 100) ? 100 : hp);
        }
        public override void buildPower(int power)
        {
            npcdefalut.setPower((power > 10) ? 10 : power);
        }
        public override void buildLevel()
        {
            npcdefalut.setLevel(2);
        }

        public override void buildClassType(string classType)
        {

            npcdefalut.setclassType((Array.IndexOf(npcdefalut.myClassArrAllay, classType) != -1) ? classType : npcdefalut.myClassArrAllay[4].ToString());
        }
        public override void buildWeapons(string weapons)
        {
            npcdefalut.setweapons(weapons);
        }

        public override void bulidFrendly()
        {
            npcdefalut.setFrendly("true");
        }
    }

    class MasterGame
    {

        private NPCCreator npccreator;

        public void setCreate(NPCCreator npccreator)
        {
            this.npccreator = npccreator;
        }

        public NPC getCreaterNPC()
        {
            return npccreator.getCreaterNPC();
        }

        public void create(string name, int hp, int power, string classType, string wepons)
        {
            npccreator.newNPC();
            npccreator.buildName(name);
            npccreator.buildHP(hp);
            npccreator.buildPower(power);
            npccreator.buildLevel();
            npccreator.buildClassType(classType);
            npccreator.buildWeapons(wepons);
            npccreator.bulidFrendly();
        }
    }
    

}
