﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns
{

    public static class Singleton1
    {
        private static string value_ = "Default variable from Singleton1";

        public static string Property
        {
            get { return value_; }
            set { value_ = value; }
        }
        public static void Method()
        {
            Console.WriteLine(value_);
        }
    }


    class Singleton2
    {
        private static Singleton2 istSigle = null;
        private static string value_ = "Default variable from Singleton2";

        private Singleton2() { }

        public static Singleton2 IstSigle()
        {
            if (istSigle == null)
            {
                istSigle = new Singleton2();
                return istSigle;
            }
            Console.WriteLine("Singleton laready exist!");
            return istSigle;
        }
        public  string Property
        {
            get { return value_; }
            set { value_ = value; }
        }

        public static void Method()
        {
            Console.WriteLine(value_);
        }

    }
}
