﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns
{
    /* STANDARD NPC STATUS */
    abstract class NPCStats
    {
        public string npcstatus = "Default NPC";

        virtual public String about()
        {
            return npcstatus;
        }

        public abstract double hp();
        public abstract double spped();
        public abstract double stenght();
        public abstract double agility();
        //public abstract void status();

    }

    /*  DCECORATE */
    abstract class Status : NPCStats
    {
        public abstract override String about();
    }

    /* teraz mamy 2 przykladowe marki samochodow */
    class EnemyStatus : NPCStats
    {

        public EnemyStatus()
        {
            npcstatus = "Enemy";
        }

        public override double hp()
        {
            return 150;
        }

        public override double spped()
        {
            return 10;
        }
        public override double stenght()
        {
            return 10;
        }
        public override double agility()
        {
            return 5;
        }
    }

    class AllayStatus : NPCStats
    {

        public AllayStatus()
        {
            npcstatus = "Allay";
        }

        public override double hp()
        {
            return 100;
        }

        public override double spped()
        {
            return 20;
        }
        public override double stenght()
        {
            return 10;
        }
        public override double agility()
        {
            return 5;
        }
    }

    /* czas na same dodatki */
    class Light : Status
    {
        NPCStats npcStat;

        public Light(NPCStats npcStat)
        {
       
            this.npcStat = npcStat;
        }

        public override String about()
        {
            return npcStat.about() + " + Use light";
        }

        public override double hp()
        {
            if (npcStat.npcstatus == "Enemy")
                return npcStat.hp() - 5;
            else
                return npcStat.hp() + 5;
        }
        public override double spped()
        {
                return npcStat.spped() + 5;
        }
        public override double stenght()
        {
                return npcStat.stenght() + 1;
        }
        public override double agility()
        {
                return npcStat.agility() + 5;
        }
    }

    class FireBall : Status
    {
        NPCStats npcStat;

        public FireBall(NPCStats npcStat)
        {
            this.npcStat = npcStat;
        }

        public override String about()
        {
            return npcStat.about() + " + Use fireball";
        }

        public override double hp()
        {
                return npcStat.hp() - 15;
        }
        public override double spped()
        {
            return npcStat.spped() + 5;
        }
        public override double stenght()
        {
                return npcStat.stenght() + 5;
        }
        public override double agility()
        {
            return npcStat.agility() - 5;
        }
       
        }

    }





